package com.samuelHCB.SchoolManagementSystem.course;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service

public interface CourseService {
    Course addCourse(CourseDto courseDto);
    List<Course> getAllCourse();
    Course getCourseByName(String name);


}
