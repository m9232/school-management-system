package com.samuelHCB.SchoolManagementSystem.course;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class CourseDto implements Serializable {
    private final String courseCode;
    private final String name;
    private final String title;
    private final String duration;
    private final Long level;
}
