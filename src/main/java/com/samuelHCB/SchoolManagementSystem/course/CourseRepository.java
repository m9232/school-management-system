package com.samuelHCB.SchoolManagementSystem.course;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {



    Course findByCourseName(String name);
    @Query("select c from Course c where courseName in :courses")
    List<Course> findByCourseNameIn(@Param("courses") List<String> course);


}