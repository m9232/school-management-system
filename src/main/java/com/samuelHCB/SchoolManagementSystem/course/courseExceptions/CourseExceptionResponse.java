package com.samuelHCB.SchoolManagementSystem.course.courseExceptions;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class CourseExceptionResponse {
    private String error ;
    private String message;
    private int statusCode;
    private HttpStatus httpStatus;
}
