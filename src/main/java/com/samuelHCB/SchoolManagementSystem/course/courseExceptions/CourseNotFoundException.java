package com.samuelHCB.SchoolManagementSystem.course.courseExceptions;

public class CourseNotFoundException extends CourseException{
    public CourseNotFoundException(String message) {
        super(message);
    }
}
