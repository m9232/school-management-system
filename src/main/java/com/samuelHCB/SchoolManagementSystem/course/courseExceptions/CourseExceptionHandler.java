package com.samuelHCB.SchoolManagementSystem.course.courseExceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class CourseExceptionHandler {

    @ExceptionHandler(CourseNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<CourseExceptionResponse>  CourseNotFoundHandler(CourseNotFoundException ex){
        CourseExceptionResponse error = new CourseExceptionResponse();
        error.setError(HttpStatus.NOT_FOUND.name());
        error.setMessage(ex.getMessage());
        error.setStatusCode(HttpStatus.NOT_FOUND.value());
        error.setHttpStatus(HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(error,HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(CourseAlreadyExistException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ResponseEntity<CourseExceptionResponse>  CourseAlreadyExistExceptionHandler(CourseAlreadyExistException ex){
        CourseExceptionResponse error = new CourseExceptionResponse();
        error.setError(HttpStatus.CONFLICT.name());
        error.setMessage(ex.getMessage());
        error.setStatusCode(HttpStatus.CONFLICT.value());
        error.setHttpStatus(HttpStatus.CONFLICT);

        return new ResponseEntity<>(error,HttpStatus.CONFLICT);
    }
}
