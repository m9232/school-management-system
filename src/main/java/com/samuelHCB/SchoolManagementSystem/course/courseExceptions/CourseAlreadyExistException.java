package com.samuelHCB.SchoolManagementSystem.course.courseExceptions;

public class CourseAlreadyExistException extends CourseException{
    public CourseAlreadyExistException(String message) {
        super(message);
    }
}
