package com.samuelHCB.SchoolManagementSystem.course.courseExceptions;

public abstract class CourseException extends RuntimeException{
    public CourseException(String message) {
        super(message);
    }
}
