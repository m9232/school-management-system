package com.samuelHCB.SchoolManagementSystem.lectures;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
public class LecturesDto implements Serializable {
    private final String fullName;
    private final String email;
    private final String mobileNumber;
    private final String nationality;
    private final String stateOfOrigin;
    private final String localGovernmentArea;
    private final String gender;
    private final Date yearOfInception;
}
