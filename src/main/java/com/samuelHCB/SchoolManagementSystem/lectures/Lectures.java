package com.samuelHCB.SchoolManagementSystem.lectures;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.samuelHCB.SchoolManagementSystem.college.College;
import com.samuelHCB.SchoolManagementSystem.course.Course;

import com.samuelHCB.SchoolManagementSystem.department.Department;
import com.samuelHCB.SchoolManagementSystem.student.Student;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "lectures")
public class Lectures {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long lecturerId;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "email")
    private String email;

    @Column(name = "mobile_number")
    private String mobileNumber;

    @Column(name = "nationality")
    private String nationality;

    @Column(name = "state_of_origin")
    private String stateOfOrigin;

    @Column(name = "local_government_area")
    private String localGovernmentArea;

    @Column(name = "gender", nullable = false)
    private String gender;


    @Temporal(TemporalType.DATE)
    @Column(name = "year_of_inception")
    private Date yearOfInception;


    @Column(name = "is_hod", nullable = false)
    private Boolean isHod = false;

    @Column(name = "is_dean", nullable = false)
    private Boolean isDean = false;


    @ManyToOne
    @JoinColumn(name = "college_id")
    private College college;

    @ManyToOne
    @JoinColumn(name = "department_id")
    private Department department;

    @ManyToMany
    @JoinTable(name = "lectures_students",
            joinColumns = @JoinColumn(name = "lectures_id"),
            inverseJoinColumns = @JoinColumn(name = "students_id"))
    private List<Student> students;
    @JsonIgnore
    @ManyToMany
    @JoinTable(name = "lectures_courses",
            joinColumns = @JoinColumn(name = "lectures_id"),
            inverseJoinColumns = @JoinColumn(name = "courses_id"))
    private List<Course> courses;
}