package com.samuelHCB.SchoolManagementSystem.lectures;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/lecturer")
@RequiredArgsConstructor
@Slf4j
public class LecturerController {
    private  final LecturesService lecturesService;
    @PostMapping("/addLecturer")
    public ResponseEntity<String> addLecturer(@RequestBody LecturesDto lecturesDto,
                                              @RequestParam("collegeName")String collegeName,
                                              @RequestParam("departmentName")String departmentName){
        log.info("ADDING LECTURER",lecturesDto.getFullName());
        lecturesService.addLectures(collegeName,departmentName,lecturesDto);
        return new ResponseEntity<>("Lecturer successfully added", HttpStatus.CREATED);
    }

    @GetMapping("/getAll")
    public ResponseEntity<List<Lectures>> getAll9(){
        log.info("GETTING ALL LECTURES");
        return  new ResponseEntity<>(lecturesService.getAllLectures(),HttpStatus.FOUND);
    }

    @PostMapping("/assignCourse")
    public ResponseEntity<Lectures> assignCourses(@RequestParam("lecturerName")String lecturerName,
                                                  @RequestParam("departmentName")String departmentName,
                                                  @RequestParam("course") String course){

        log.info("Assigning course to lecture",lecturerName);
        return new ResponseEntity<>(lecturesService.assignCourses(lecturerName,departmentName,course),HttpStatus.CREATED);
    }


}
