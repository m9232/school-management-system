package com.samuelHCB.SchoolManagementSystem.lectures.lecturerException;

public class LecturerNotFoundException extends LecturerException{
    public LecturerNotFoundException(String message) {
        super(message);
    }
}
