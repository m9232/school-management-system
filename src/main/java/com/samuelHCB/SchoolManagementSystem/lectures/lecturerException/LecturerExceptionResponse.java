package com.samuelHCB.SchoolManagementSystem.lectures.lecturerException;

import lombok.Data;
import org.springframework.http.HttpStatus;
@Data
public class LecturerExceptionResponse {
    private String error ;
    private String message;
    private int statusCode;
    private HttpStatus httpStatus;
}
