package com.samuelHCB.SchoolManagementSystem.lectures.lecturerException;

public class LecturerAlreadyExistException extends  LecturerException{
    public LecturerAlreadyExistException(String message) {
        super(message);
    }
}
