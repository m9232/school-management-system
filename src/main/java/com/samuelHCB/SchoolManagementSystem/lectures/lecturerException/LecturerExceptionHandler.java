package com.samuelHCB.SchoolManagementSystem.lectures.lecturerException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class LecturerExceptionHandler {
    @ExceptionHandler(LecturerNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<LecturerExceptionResponse> collegeNotFoundExceptionResponse(LecturerNotFoundException ex){
        LecturerExceptionResponse lecturerExceptionResponse = new LecturerExceptionResponse();
        lecturerExceptionResponse.setError(HttpStatus.NOT_FOUND.name());
        lecturerExceptionResponse.setMessage(ex.getMessage());
        lecturerExceptionResponse.setStatusCode(HttpStatus.NOT_FOUND.value());
        lecturerExceptionResponse.setHttpStatus(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(lecturerExceptionResponse,HttpStatus.NOT_FOUND);

    }
    @ExceptionHandler(LecturerAlreadyExistException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ResponseEntity<LecturerExceptionResponse> collegeAlreadyExistResponse(LecturerAlreadyExistException ex){
        LecturerExceptionResponse lecturerExceptionResponse = new LecturerExceptionResponse();
        lecturerExceptionResponse.setError(HttpStatus.CONFLICT.name());
        lecturerExceptionResponse.setMessage(ex.getMessage());
        lecturerExceptionResponse.setStatusCode(HttpStatus.CONFLICT.value());
        lecturerExceptionResponse.setHttpStatus(HttpStatus.CONFLICT);
        return new ResponseEntity<>(lecturerExceptionResponse,HttpStatus.CONFLICT);

    }

}
