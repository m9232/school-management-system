package com.samuelHCB.SchoolManagementSystem.department;

import com.samuelHCB.SchoolManagementSystem.course.Course;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/department")
@RequiredArgsConstructor
@Slf4j
public class DepartmentController {
    private final DepartmentService departmentService;

    @PostMapping("/addDepartment")
    public ResponseEntity<String> addDepartment(@RequestBody DepartmentDto departmentDto){
        log.info("adding department", departmentDto.getAlias());
        departmentService.addDepartment(departmentDto);
        return new ResponseEntity<>("department added successfully", HttpStatus.CREATED);
    }

    @GetMapping("/getAll")
    public ResponseEntity<List<Department>> getAllDepartment(){
        log.info("getting all departments");
        return new ResponseEntity<>(departmentService.getAll(),HttpStatus.FOUND);
    }
    @GetMapping("/getDepartment")
    public ResponseEntity<Department> getDepartment(@RequestParam("departmentName") String departmentName){
        log.info("getting department",departmentName);
        return new ResponseEntity<>(departmentService.getByName(departmentName),HttpStatus.FOUND);
    }

    @PostMapping("/addStudent")
    public ResponseEntity<String> addStudent(@RequestParam("departmentName") String departmentName,
                                             @RequestParam("firstName") String firstName,
                                             @RequestParam("surName") String surName){
        log.info("adding student:" ,firstName+surName ,"to department:",departmentName);
        departmentService.addStudent(departmentName,firstName,surName);
        return new ResponseEntity<>("student added successfully",HttpStatus.CREATED);

    }
    @PostMapping("/addLecturer")
    public ResponseEntity<Department> addLecturer (@RequestParam("departmentName")String departmentName,
                                                   @RequestParam("lecturerName")String lecturerName){
        log.info("Adding lecturer to;:", departmentName);
        return new ResponseEntity<>(departmentService.addLecturer(departmentName,lecturerName), HttpStatus.CREATED);
    }
    @PostMapping("/addCourse")
    public ResponseEntity<Department> addCourse(@RequestParam("departmentName")String departmentName,
                                                @RequestBody List<String> courses){
        log.info("adding course to department",departmentName);
        return  new ResponseEntity<>(departmentService.addCourse(departmentName,courses),HttpStatus.CREATED);

    }


}
