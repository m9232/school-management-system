package com.samuelHCB.SchoolManagementSystem.department;

import com.samuelHCB.SchoolManagementSystem.course.Course;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface DepartmentService {
    Department addDepartment(DepartmentDto departmentDto);

    List<Department> getAll();

    Department getByName(String departmentName);

    Department addStudent(String departmentName,String firstName, String surName);
    Department addHod (String departmentName,String hodName);

    Department addCourse(String departmentName, List<String> courses);


    Department addLecturer(String departmentName,String lectureName);

}
