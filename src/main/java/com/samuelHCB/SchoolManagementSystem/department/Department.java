package com.samuelHCB.SchoolManagementSystem.department;

import com.samuelHCB.SchoolManagementSystem.college.College;
import com.samuelHCB.SchoolManagementSystem.course.Course;
import com.samuelHCB.SchoolManagementSystem.dean.Deans;
import com.samuelHCB.SchoolManagementSystem.hod.Hods;
import com.samuelHCB.SchoolManagementSystem.lectures.Lectures;
import com.samuelHCB.SchoolManagementSystem.student.Student;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "department")
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long departmentId;


    @Column(name = "name")
    private String name;

    @Temporal(TemporalType.DATE)
    @Column(name = "year_created")
    private Date yearCreated;

    @Column(name = "admitting_capacity")
    private Integer admittingCapacity;

    @Column(name = "alias")
    private String alias;

    @Column(name = "course_duration")
    private String courseDuration;

    @Column(name = "course_of_study")
    private String courseOfStudy;

    @ManyToOne(cascade = CascadeType.ALL,
            fetch = FetchType.EAGER)
    @JoinTable(name = "college_department",joinColumns = @JoinColumn(name = "department_id"),
            inverseJoinColumns =@JoinColumn(name = "college_id"))
    private College college;


   @OneToMany(cascade = CascadeType.ALL,fetch =  FetchType.EAGER)
   private  List<Course> courses;



    @OneToOne( cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    private Hods hods;

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    private List<Student> students;


    @OneToMany( cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    private List<Lectures> lectures ;



}