package com.samuelHCB.SchoolManagementSystem.department.departmentExceptions;

import lombok.Data;
import org.springframework.http.HttpStatus;
@Data
public class DepartmentExceptionResponse {
    private String error ;
    private String message;
    private int statusCode;
    private HttpStatus httpStatus;
}
