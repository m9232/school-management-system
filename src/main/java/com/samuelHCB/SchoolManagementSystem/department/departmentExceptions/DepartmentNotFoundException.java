package com.samuelHCB.SchoolManagementSystem.department.departmentExceptions;

public class DepartmentNotFoundException extends DepartmentException{
    public DepartmentNotFoundException(String message) {
        super(message);
    }
}
