package com.samuelHCB.SchoolManagementSystem.department.departmentExceptions;

public class DepartmentAlreadyExist extends DepartmentException{
    public DepartmentAlreadyExist(String message) {
        super(message);
    }
}
