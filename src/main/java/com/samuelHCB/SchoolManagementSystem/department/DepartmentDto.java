package com.samuelHCB.SchoolManagementSystem.department;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
public class DepartmentDto implements Serializable {
    private final String name;
    private final Date yearCreated;
    private final Integer admittingCapacity;
    private final String alias;
    private final String courseDuration;
    private final String courseOfStudy;
}
