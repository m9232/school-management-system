package com.samuelHCB.SchoolManagementSystem.student;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class StudentDto implements Serializable {
    private final String firstName;
    private final String middleName;
    private final String surName;
    private final Integer level;
    private final Long mobileNumber;
    private final String nationality;
    private final String stateOfOrigin;
    private final String localGovernmentArea;
    private final String gender;
    private final String admissionYear;
    private final String courseDuration;
}
