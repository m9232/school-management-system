package com.samuelHCB.SchoolManagementSystem.student;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface StudentService {
    Student addStudent (String collegeName,String departmentName,StudentDto studentDto);
    List<Student> getAll();
    Student findByFirstNameAndSurName(String firstName, String surName);
    Student assignCollege(String collegeName, String firstName, String surName);

    Student courseRegistration(String name,String surname,String collegeName, String departmentName, List<String> courses);
}
