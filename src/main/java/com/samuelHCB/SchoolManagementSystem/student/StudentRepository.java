package com.samuelHCB.SchoolManagementSystem.student;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student, Long> {
    Student findByFirstNameAndSurName(String firstName, String surName);

}