package com.samuelHCB.SchoolManagementSystem.student.studentExceptions;

public abstract class StudentException extends RuntimeException {
    public StudentException(String message) {
        super(message);
    }
}
