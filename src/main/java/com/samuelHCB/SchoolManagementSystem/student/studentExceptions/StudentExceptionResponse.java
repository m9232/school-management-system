package com.samuelHCB.SchoolManagementSystem.student.studentExceptions;

import lombok.Data;
import org.springframework.http.HttpStatus;
@Data
public class StudentExceptionResponse {
    private String error ;
    private String message;
    private int statusCode;
    private HttpStatus httpStatus;
}
