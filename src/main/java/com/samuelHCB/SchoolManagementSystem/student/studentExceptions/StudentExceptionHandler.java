package com.samuelHCB.SchoolManagementSystem.student.studentExceptions;

import com.samuelHCB.SchoolManagementSystem.college.collegeExceptions.CollegeAlreadyExistException;
import com.samuelHCB.SchoolManagementSystem.department.departmentExceptions.DepartmentAlreadyExist;
import com.samuelHCB.SchoolManagementSystem.department.departmentExceptions.DepartmentExceptionResponse;
import com.samuelHCB.SchoolManagementSystem.department.departmentExceptions.DepartmentNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class StudentExceptionHandler {
    @ExceptionHandler(StudentNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<StudentExceptionResponse> studentNotFoundExceptionResponse(DepartmentNotFoundException ex) {
        StudentExceptionResponse studentExceptionResponse = new StudentExceptionResponse();
        studentExceptionResponse.setError(HttpStatus.NOT_FOUND.name());
        studentExceptionResponse.setMessage(ex.getMessage());
        studentExceptionResponse.setStatusCode(HttpStatus.NOT_FOUND.value());
        studentExceptionResponse.setHttpStatus(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(studentExceptionResponse, HttpStatus.NOT_FOUND);

    }

    @ExceptionHandler(StudentAlreadyExistException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ResponseEntity<StudentExceptionResponse> studentAlreadyExistResponse(CollegeAlreadyExistException ex) {
        StudentExceptionResponse StudentExceptionResponse = new StudentExceptionResponse();
        StudentExceptionResponse.setError(HttpStatus.CONFLICT.name());
        StudentExceptionResponse.setMessage(ex.getMessage());
        StudentExceptionResponse.setStatusCode(HttpStatus.CONFLICT.value());
        StudentExceptionResponse.setHttpStatus(HttpStatus.CONFLICT);
        return new ResponseEntity<>(StudentExceptionResponse, HttpStatus.CONFLICT);

    }
}
