package com.samuelHCB.SchoolManagementSystem.student.studentExceptions;

public class StudentNotFoundException extends StudentException{
    public StudentNotFoundException(String message) {
        super(message);
    }
}
