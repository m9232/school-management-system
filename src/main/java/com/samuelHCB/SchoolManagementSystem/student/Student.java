package com.samuelHCB.SchoolManagementSystem.student;

import com.samuelHCB.SchoolManagementSystem.college.College;
import com.samuelHCB.SchoolManagementSystem.course.Course;
import com.samuelHCB.SchoolManagementSystem.dean.Deans;
import com.samuelHCB.SchoolManagementSystem.department.Department;
import com.samuelHCB.SchoolManagementSystem.hod.Hods;
import com.samuelHCB.SchoolManagementSystem.lectures.Lectures;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "student")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long studentId;


    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "sur_name", nullable = false)
    private String surName;

    @Column(name = "middle_name")
    private String middleName;
    @Column(name = "level", nullable = false)
    private Integer level;

    @Column(name = "mobile_number")
    private Long mobileNumber;

    @Column(name = "nationality")
    private String nationality;

    @Column(name = "state_of_origin")
    private String stateOfOrigin;

    @Column(name = "local_government_area")
    private String localGovernmentArea;

    @Column(name = "gender", nullable = false)
    private String gender;

    @Column(name = "admission_year")
    private String admissionYear;

    @Column(name = "course_duration")
    private String courseDuration;

    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinColumn(name = "college_Id",referencedColumnName = "collegeId")
    private College college;

    @ManyToMany
    @JoinTable(name = "student_courses",
            joinColumns = @JoinColumn(name = "student_id"),
            inverseJoinColumns = @JoinColumn(name = "courses_id"))
    private List<Course> courses ;

    @ManyToOne(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    private Department department;

    @ManyToMany(mappedBy = "students",
            fetch = FetchType.LAZY)
    private List<Lectures> lectures ;

    @ManyToOne(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    private Deans deans;

    @ManyToOne(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    private Hods hods;

}