package com.samuelHCB.SchoolManagementSystem.hod;

import com.samuelHCB.SchoolManagementSystem.college.College;
import com.samuelHCB.SchoolManagementSystem.course.Course;
import com.samuelHCB.SchoolManagementSystem.department.Department;
import com.samuelHCB.SchoolManagementSystem.lectures.Lectures;
import com.samuelHCB.SchoolManagementSystem.student.Student;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "hods")
public class Hods {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "email")
    private String email;

    @Column(name = "mobile_number")
    private Integer mobileNumber;

    @Column(name = "nationality")
    private String nationality;

    @Column(name = "state_of_origin")
    private String stateOfOrigin;

    @Column(name = "local_government_area")
    private String localGovernmentArea;

    @Column(name = "gender", nullable = false)
    private String gender;


    @Column(name = "year_of_inception")
    private String yearOfInception;

    @ManyToOne
    @JoinColumn(name = "college_id",referencedColumnName = "collegeId")
    private College college;


    @OneToOne( cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    private Lectures lectures;

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    private List<Course> courses ;


    @OneToMany( cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    private List<Student> students;

}