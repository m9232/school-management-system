package com.samuelHCB.SchoolManagementSystem.college;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CollegeRepository extends JpaRepository<College, Long> {
    College findByName(String name);
    List<College> findAllByNameIn(List<String> collegeName);

    College deleteByName(String collegeName);
}
