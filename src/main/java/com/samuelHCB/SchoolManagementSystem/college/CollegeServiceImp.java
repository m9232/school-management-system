package com.samuelHCB.SchoolManagementSystem.college;

import com.samuelHCB.SchoolManagementSystem.college.collegeExceptions.CollegeAlreadyExistException;
import com.samuelHCB.SchoolManagementSystem.college.collegeExceptions.CollegeNotFoundException;
import com.samuelHCB.SchoolManagementSystem.course.Course;
import com.samuelHCB.SchoolManagementSystem.course.CourseRepository;
import com.samuelHCB.SchoolManagementSystem.course.courseExceptions.CourseNotFoundException;
import com.samuelHCB.SchoolManagementSystem.dean.Deans;
import com.samuelHCB.SchoolManagementSystem.dean.DeansRepository;
import com.samuelHCB.SchoolManagementSystem.department.Department;
import com.samuelHCB.SchoolManagementSystem.department.DepartmentRepository;
import com.samuelHCB.SchoolManagementSystem.department.departmentExceptions.DepartmentAlreadyExist;
import com.samuelHCB.SchoolManagementSystem.department.departmentExceptions.DepartmentNotFoundException;
import com.samuelHCB.SchoolManagementSystem.hod.Hods;
import com.samuelHCB.SchoolManagementSystem.hod.HodsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor

public class CollegeServiceImp implements CollegeService{
    @Autowired
    private final CollegeRepository collegeRepository;
    @Autowired
    private DeansRepository deansRepository;
    @Autowired
    private HodsRepository hodsRepository;
    @Autowired
    private DepartmentRepository departmentRepository;
    @Autowired
    private CourseRepository courseRepository;



    @Override
    public College addCollege(CollegeDto collegeDto) {
        College college = new College();
        College collegeCheck = collegeRepository.findByName(collegeDto.getName());

        if (collegeCheck!=null){
            throw new CollegeAlreadyExistException("This college already exist");
        }else {
           college.setName(collegeDto.getName());
           college.setAdmittingCapacity(collegeDto.getAdmittingCapacity());
           college.setAlias(collegeDto.getAlias());
           college.setDepartmentSize(collegeDto.getDepartmentSize());
            return collegeRepository.save(college);

        }
    }

    @Override
    public List<College> getAllCollege() {
        List<College> collegeList = collegeRepository.findAll();
        return collegeList;
    }
    @Override
    public College getCollegeByName(String name) {
            College college = collegeRepository.findByName(name);
            if (college.equals(null)){
                throw new CollegeNotFoundException("This college doest exist:");

            }
            return college;
    }


    @Override
    public College deleteByName(String collegeName) {
            College college = collegeRepository.findByName(collegeName);
            if (college.equals(null)){
                throw new CollegeNotFoundException("This college doest exist:");
            }
            collegeRepository.delete(college);
            return college;

    }


    public College addCourseToCollege(String collegeName, List<String> courseName) {
        College college = collegeRepository.findByName(collegeName);
        if (college.equals(null)){
            throw new CollegeNotFoundException("College with not found");
        }
        List<Course> currentCollegeCoursesList = college.getCourses();
        List<Course> courses = courseRepository.findByCourseNameIn(courseName);
        if (courses.equals(null)) {
            throw new CourseNotFoundException("Course not present");
        }else {
            courses.removeAll(currentCollegeCoursesList);
            college.setCourses(courses);
            return collegeRepository.save(college);

        }
    }

    @Override
    public College update(String collegeName, CollegeDto collegeDto) {
        College college = collegeRepository.findByName(collegeName);
        if (college == null){
            throw new CollegeNotFoundException("This college doesn't exist" + collegeName);
        }else {
            college.setName(collegeDto.getName());
            college.setAlias(collegeDto.getAlias());
            college.setYearCreated(collegeDto.getYearCreated());
            college.setDepartmentSize(collegeDto.getDepartmentSize());
            college.setAdmittingCapacity(collegeDto.getAdmittingCapacity());
            return collegeRepository.save(college);
        }
    }

    @Override
    public College correctData(String collegeName, CollegeDto collegeDto) {
        College college = collegeRepository.findByName(collegeName);
        if (college == null) {
            throw new CollegeNotFoundException("This college doesn't exist" + collegeName);
        }
        if (collegeDto.getName()!=null){
            college.setName(collegeDto.getName());
        }if (collegeDto.getAlias()!=null){
            college.setAlias(collegeDto.getAlias());
        }if (collegeDto.getDepartmentSize()!=null){
            college.setDepartmentSize(collegeDto.getDepartmentSize());
        }if ((collegeDto.getAdmittingCapacity()!=null)){
            college.setAdmittingCapacity(collegeDto.getAdmittingCapacity());
        }if (collegeDto.getYearCreated()!=null){
            college.setYearCreated(collegeDto.getYearCreated());
        }

        return collegeRepository.save(college);
    }


    @Override
    public College addDeanToCollege(String collegeName, String deanName) {
       College college = collegeRepository.findByName(collegeName);
       Deans dean = deansRepository.findByFullName(deanName);
        if (college==null){
            throw new CollegeNotFoundException("College with not found");

        }else if(dean==null) {
            throw  new DepartmentNotFoundException("this Dean does not exist");
        }else {
            college.setDeans(dean);
           return collegeRepository.save(college);
        }

    }


    @Override
    public College addHodsToCollege(String collegeName,List<String> hodName) {
        College college = collegeRepository.findByName(collegeName);
        List<Hods> hods = hodsRepository.findAllByFullNameIn(hodName);
        if (college==null){
            throw new CollegeNotFoundException("College with not found");
        }else if (hods.size()==0){
            throw new CollegeNotFoundException("this hod could not be found");

        }else {
            college.setHods(hods);
            return collegeRepository.save(college);
        }

    }

    @Override
    public College addDepartmentsCollege(String collegeName, List<String> departmentName) {
        College college = collegeRepository.findByName(collegeName);
        if (college == null) {

            throw new CollegeNotFoundException("College not found");
        }
        List<Department> departments = departmentRepository.findAllByNameIn(departmentName);
        if(departments.size()==0) {
            throw new DepartmentNotFoundException("no departments found");
        }
        List<Department>  departmentInCollege = college.getDepartments();
        departments.removeAll(departmentInCollege);
        college.setDepartments( departments);
            return collegeRepository.save(college);

        }


}
