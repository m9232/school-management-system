package com.samuelHCB.SchoolManagementSystem.college;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service
public interface CollegeService {
     College addCourseToCollege(String collegeName, List<String> courseName);

     College update (String collegeName,CollegeDto collegeDto);
     College correctData(String collegeName, CollegeDto collegeDto);


     College addDeanToCollege(String collegeName, String deanName);

    College addHodsToCollege(String collegeName, List<String> hodName);
    College addDepartmentsCollege(String collegeName, List<String> departmentName);

    College addCollege(CollegeDto collegeDto);
    List<College> getAllCollege();
    College getCollegeByName(String name);


    College deleteByName(String collegeName);
}
