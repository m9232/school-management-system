package com.samuelHCB.SchoolManagementSystem.college.collegeExceptions;
public abstract class CollegeException extends RuntimeException{
    public CollegeException(String message) {
        super(message);
    }
}
