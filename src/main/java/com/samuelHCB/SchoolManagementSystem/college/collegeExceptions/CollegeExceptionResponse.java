package com.samuelHCB.SchoolManagementSystem.college.collegeExceptions;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class CollegeExceptionResponse {
    private String error ;
    private String message;
    private int statusCode;
    private HttpStatus httpStatus;
}
