package com.samuelHCB.SchoolManagementSystem.college.collegeExceptions;

public class CollegeAlreadyExistException extends CollegeException{
    public CollegeAlreadyExistException(String message) {
        super(message);
    }
}
