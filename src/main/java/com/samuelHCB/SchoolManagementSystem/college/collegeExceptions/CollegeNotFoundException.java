package com.samuelHCB.SchoolManagementSystem.college.collegeExceptions;

public class CollegeNotFoundException extends CollegeException {
    public CollegeNotFoundException(String message) {
        super(message);
    }
}
