package com.samuelHCB.SchoolManagementSystem.college.collegeExceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class CollegeExceptionHandler {

    @ExceptionHandler(CollegeNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<CollegeExceptionResponse> collegeNotFoundExceptionResponse(CollegeNotFoundException ex){
        CollegeExceptionResponse collegeExceptionResponse = new CollegeExceptionResponse();
        collegeExceptionResponse.setError(HttpStatus.NOT_FOUND.name());
        collegeExceptionResponse.setMessage(ex.getMessage());
        collegeExceptionResponse.setStatusCode(HttpStatus.NOT_FOUND.value());
        collegeExceptionResponse.setHttpStatus(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(collegeExceptionResponse,HttpStatus.NOT_FOUND);

    }
    @ExceptionHandler(CollegeAlreadyExistException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ResponseEntity<CollegeExceptionResponse> collegeAlreadyExistResponse(CollegeAlreadyExistException ex){
        CollegeExceptionResponse collegeExceptionResponse = new CollegeExceptionResponse();
        collegeExceptionResponse.setError(HttpStatus.CONFLICT.name());
        collegeExceptionResponse.setMessage(ex.getMessage());
        collegeExceptionResponse.setStatusCode(HttpStatus.CONFLICT.value());
        collegeExceptionResponse.setHttpStatus(HttpStatus.CONFLICT);
        return new ResponseEntity<>(collegeExceptionResponse,HttpStatus.CONFLICT);

    }
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex){
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error)->{
            String fieldName= ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName,errorMessage);
        });
        return errors;
    }
}
