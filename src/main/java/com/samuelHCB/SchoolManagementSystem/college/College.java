package com.samuelHCB.SchoolManagementSystem.college;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.samuelHCB.SchoolManagementSystem.course.Course;
import com.samuelHCB.SchoolManagementSystem.dean.Deans;
import com.samuelHCB.SchoolManagementSystem.department.Department;
import com.samuelHCB.SchoolManagementSystem.hod.Hods;
import com.samuelHCB.SchoolManagementSystem.lectures.Lectures;
import com.samuelHCB.SchoolManagementSystem.student.Student;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "college")
public class College {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long collegeId;

    @Column(name = "name")

    private String name;

    @Column(name = "alias")
    private String alias;

    @Column(name = "admitting_capacity")
    private Integer admittingCapacity;

    @Column(name = "department_size")
    private Integer departmentSize;

    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "year_created")
    private Date yearCreated;

    @OneToMany( cascade = CascadeType.ALL,
    fetch = FetchType.LAZY, mappedBy = "college")
    private List<Department> departments;

    @OneToMany( cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,mappedBy = "college")
    private List<Student> students ;



    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,mappedBy = "college")
    private List<Lectures> lectures;

    @OneToMany( cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,mappedBy = "college")
    private List<Hods> hods;

    @OneToOne( cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    private Deans deans;

    @OneToMany( cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,mappedBy = "college")
    private List<Course> courses ;

}