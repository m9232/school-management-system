package com.samuelHCB.SchoolManagementSystem.college;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
@RequestMapping("/college")
@Slf4j
@RequiredArgsConstructor
public class CollegeController {
    private final CollegeService collegeService;

    @PostMapping("/addCollege")
    public ResponseEntity<String> addCollege( @RequestBody CollegeDto collegeDto){
        log.info("adding college::",collegeDto.getName());
        collegeService.addCollege(collegeDto);
        return new ResponseEntity<>("college added successfully", HttpStatus.CREATED);
    }
    @GetMapping("/getCollege")
    public ResponseEntity<College> getByName(@RequestParam("collegeName") String collegeName){

        return new ResponseEntity<>( collegeService.getCollegeByName(collegeName),HttpStatus.FOUND);
    }

    @GetMapping("/getAll")
    public ResponseEntity<List<College>> getAll(){
        return new ResponseEntity<>(collegeService.getAllCollege(),HttpStatus.FOUND);
    }

    @PostMapping("/addDepartment")
    public ResponseEntity<String> addDepartmentToCollege(@RequestParam("collegeName") String collegeName,
                                                         @RequestBody List<String> departments){
        log.info("Adding Department to college");

             collegeService.addDepartmentsCollege(collegeName,departments);
             return new ResponseEntity<>("Department successfully added",HttpStatus.CREATED);
    }

    @PostMapping("/addCourse")
    public ResponseEntity<String> addCourseToCollege(@RequestParam("collegeName") String collegeName,
                                                    @RequestBody List<String> courseName){
        log.info("Adding Course to College");
        collegeService.addCourseToCollege(collegeName,courseName);
        return new ResponseEntity<>("Course Successfully Added",HttpStatus.CREATED);
    }

    @PutMapping("/update")
    public ResponseEntity<College> updateCollege(@RequestParam("collegeName") String collegeName,
                                                 @RequestBody CollegeDto collegeDto){
        log.info("Updating college: ", collegeName + "with " + collegeDto.getName());
        return  new ResponseEntity<>(collegeService.update(collegeName,collegeDto),HttpStatus.CREATED);
    }

    @PatchMapping("/correctData")
    public ResponseEntity<College> correctCollegeData(@RequestParam("collegeName") String collegeName,
                                                     @RequestBody CollegeDto collegeDto){
        log.info("correcting data for ",collegeName,collegeDto.getName());
        return new ResponseEntity<>(collegeService.correctData(collegeName,collegeDto),HttpStatus.CREATED);
    }
    @DeleteMapping("/delete")
    public ResponseEntity<String> deleteByName(@RequestParam("collegeName") String collegeName){
        log.info("deleting college:", collegeName);
        collegeService.deleteByName(collegeName);
        return  new ResponseEntity<>("deleted successfully",HttpStatus.CREATED);
    }
}
