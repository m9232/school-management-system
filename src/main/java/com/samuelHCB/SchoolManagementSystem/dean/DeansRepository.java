package com.samuelHCB.SchoolManagementSystem.dean;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface DeansRepository extends JpaRepository<Deans, Long> {
  Deans findByFullName(String deanName);

    List<Deans> findAllByFullNameIn(List<String> deanName);
}